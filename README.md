# phpMyAdmin

Forked version of phpMyAdmin with ClearOS changes applied

* git clone git@gitlab.com:clearos/clearfoundation/phpMyAdmin.git
* cd phpMyAdmin
* git checkout epel7
* git remote add upstream https://src.fedoraproject.org/rpms/phpMyAdmin.git
* git pull upstream epel7
* git checkout clear7
* git merge --no-commit epel7
* git commit
